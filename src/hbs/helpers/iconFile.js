module.exports = function (eventType) {
    switch (eventType) {
        case 'java':
            return 'workshop-java-icon';
        case 'docker':
            return 'workshop-docker-icon';
        case 'cs':
            return 'workshop-csharp-icon';
        case 'js':
            return 'workshop-js-icon';
        case 'private':
            return 'workshop-private-icon';
        default:
            return '';
    }
}
