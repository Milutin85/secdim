module.exports = function (eventTypes) {
    if (eventTypes.includes('private')) {
        return 'btn-primary';
    }
    return 'btn-outline-primary'
}
