module.exports = function (eventTypes) {
    if (eventTypes.includes('private')) {
        return 'Book Demo';
    }
    return 'Get Started'
}
