import 'popper.js';
import 'bootstrap';
import './scss/styles.scss';

import Prism from 'prismjs';
import 'paginationjs';

let workshopTemplate = require('./hbs/handlebars/workshop.handlebars');

// Import PrismJS extensions
(function () {
    'use strict';

    $(function () {


        // Workshops
        $('.workshop-pagination').pagination({
            dataSource: function (done) {

                $.ajax({
                    type: 'GET',
                    url: 'code/workshop.json',
                    success: function (response) {
                        done(response.events);
                    }
                })

            },
            pageSize: 6,
            ulClassName: 'pagination justify-content-center',
            prevText: '&lsaquo;',
            nextText: '&rsaquo;',
            callback: function (data, pagination) {

                const html = workshopTemplate({
                    workshops: data
                });

                $('.workshops-items .row').html(html);

                // Workshops tooltips
                $('[data-toggle="popover"]').popover({
                    html: true,
                    content: function () {
                        const content = $(this).attr('data-popover-content');
                        return $(content).children('.popover-body').html();
                    }
                })
            }
        });


        // Prism JS
        setTimeout(() => {
            Prism.highlightAll();
        }, 0);

        // Defensive by design menu
        const $menuItemsContainer = $('.snippets-code__languages');
        const $snippets = $('.snippets-code__wrap');

        if ($menuItemsContainer) {

            $menuItemsContainer.on('click', 'a', function (event) {
                const $self = $(this);
                event.preventDefault();

                $menuItemsContainer.find('li').removeClass('active');
                $self.parent('li').addClass('active');


                const snippetLang = $self.data('snippet');

                $snippets.each(function (index, item) {
                    $(item).addClass('d-none');
                });

                $snippets.filter(`[data-code='${snippetLang}']`).removeClass('d-none');

            });
        }
    });



}());
