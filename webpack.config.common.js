const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    entry: './src/app.js',
    output: {
        filename: '[name].[contenthash].js',
        path: path.resolve(__dirname, 'dist')
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            minify: {
                collapseWhitespace: false
            },
            filename: 'index.html',
            inject: 'body',
            template: path.resolve(__dirname, 'src', 'index.html')
        }),
        new HtmlWebpackPlugin({
            minify: {
                collapseWhitespace: false
            },
            filename: 'workshop/index.html',
            inject: 'body',
            template: path.resolve(__dirname, 'src/workshop', 'index.html')
        }),
        new HtmlWebpackPlugin({
            minify: {
                collapseWhitespace: false
            },
            filename: 'workshop2/index.html',
            inject: 'body',
            template: path.resolve(__dirname, 'src/workshop2', 'index.html')
        }),
        new HtmlWebpackPlugin({
            minify: {
                collapseWhitespace: false
            },
            filename: 'platform/index.html',
            inject: 'body',
            template: path.resolve(__dirname, 'src/platform', 'index.html')
        }),
        new HtmlWebpackPlugin({
            minify: {
                collapseWhitespace: false
            },
            filename: 'about/index.html',
            inject: 'body',
            template: path.resolve(__dirname, 'src/about', 'index.html')
        }),
        new CopyWebpackPlugin({
            patterns: [
                { from: './src/assets', to: 'assets'},
                { from: './src/code', to: 'code'}
            ]
        }),
        new webpack.ProvidePlugin({
            'window.jQuery'    : 'jquery',
            'window.$'         : 'jquery',
            $: "jquery",
            jQuery: "jquery"
        })

    ],
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components|code)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.svg/,
                type: 'asset/inline'
            },
            {
                test: /\.(png|jpg|gif)$/i,
                type: 'asset/resource'
            },
            {
                test: /\.handlebars$/,
                loader: 'handlebars-loader',
                options: {
                    precompileOptions: false,
                    helperDirs: path.resolve(__dirname, './src/hbs/helpers')
                },

            }
        ]
    }
}

